///-///这是基于“控制台”的程序，打开控制台程序进入当前路径，运行可执行此文件，源码如下：
#include <stdio.h> ///-/// 有2*I_NUM个同学，两个一组打乒乓球，有多少种不相同的分组方法，打印出来。
#define I_NUM  3  ///-///这个数字不要修改的太大，因为结果太多，会占用很长时间。

int F_I_Divide(int *i_Data_1_In, int *i_NumCur_In, int *i_SumNum_In)
{
    int i=0, j=0, i_NumCur=*i_NumCur_In, i_Swap_Temp=0, i_Data_0[I_NUM*2], i_Temp=0, i_Max=-1;
    for(i=0; i<I_NUM*2; i++)
    {
        i_Data_0[i]=i_Data_1_In[i];
    }
    if(i_NumCur>=2)
    {
        for(i=0; i<i_NumCur; i++)
        {
            if(i_Data_0[i]>i_Max)
            {
                i_Temp=i;
                i_Max=i_Data_0[i];
            }
        }
        i_Swap_Temp=i_Data_0[i_NumCur-1];
        i_Data_0[i_NumCur-1]=i_Data_0[i_Temp];
        i_Data_0[i_Temp]=i_Swap_Temp;
        for(j=i_NumCur-2; j>=0; j--)
        {
            i_Swap_Temp=i_Data_0[i_NumCur-2];
            i_Data_0[i_NumCur-2]=i_Data_0[j];
            i_Data_0[j]=i_Swap_Temp;
            *i_NumCur_In=i_NumCur-2;
            F_I_Divide(i_Data_0, i_NumCur_In, i_SumNum_In);
        }
    }
    else
    {
        for(i=0; i<I_NUM; i++)
        {
            printf("[%3d,%3d] ", i_Data_0[2*i], i_Data_0[2*i+1]);
        }
        printf("\r\n");
        *i_SumNum_In=(*i_SumNum_In)+1;
    }
    return 0;
}

int main()
{
    int i=0, i_Num=I_NUM*2, i_SumNum=0, i_Data_1[I_NUM*2];
    for(i=0; i<i_Num; i++)
    {
        i_Data_1[i]=i;
    }
    F_I_Divide(i_Data_1, &i_Num, &i_SumNum);
    printf("There are %d results.\r\n", i_SumNum);
    return 0;
}

